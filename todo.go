// todo.go
package main

import (
	"database/sql"
	"github.com/amaslenn/go-echo-vue/handlers"

	"github.com/labstack/echo"
	//"github.com/labstack/echo/engine/standard"
	_ "github.com/mattn/go-sqlite3"
)

func initDB(filepath string) *sql.DB {
	db, err := sql.Open("sqlite3", filepath)
	if err != nil {
		panic(err)
	}

	if db == nil {
		panic("db nil")
	}

	return db
}

func migrate(db *sql.DB) {
	sql := `
	CREATE TABLE IF NOT EXISTS tasks(
		id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
		name VARCHAR NOT NULL
	);
	`

	_, err := db.Exec(sql)
	if err != nil {
		panic(err)
	}
}

func main() {
	db := initDB("storage.db")
	migrate(db)

	// Create a new instance of Echo
	e := echo.New()

	e.File("/", "/home/amaslenn/0_work/go/src/github.com/amaslenn/go-echo-vue/public/index.html")
	e.GET("/tasks", handlers.GetTasks(db))
	e.PUT("/tasks", handlers.PutTask(db))
	e.DELETE("/tasks/:id", handlers.DeleteTask(db))

	// Start as a web server
	//e.Run(standard.New(":8000"))
	e.Logger.Fatal(e.Start(":8000"))
}
